# Controles
Controles por teclado:

| Teclas | Acción |
| ------ | ------ |
|W/A/S/D      |`Mover al personaje`  |
|SPACE / ENTER   |`Saltar`            |

## Ampliaciones

Joints en las puertas y en la plataforma afectada por el peso del personaje.
Materiales y algunos assets de la AssetStore.
